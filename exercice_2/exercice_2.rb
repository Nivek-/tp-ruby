# Création d'un tableau
arr = Array.new

# Ajout des valeurs allant de 1 à 100 dans le tableau
(1..100).each do |n|
    arr.push(n)
end

# Affichage des valeurs du tableau comprises entre 30 et 59
for n in arr do
    if n > 30 && n < 59
        puts n
    end
end