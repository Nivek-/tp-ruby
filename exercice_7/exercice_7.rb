
def game
    # Tableau contenant les mots à sélectionner
    words = ["paella", "alarme", "place", "cerise", "aigle", "autonome", "statue", "bouillir", "mafia", "porte", "livre", "blague", "flamber", "crevette", "amour", "scalpel", "masquer", "parfait", "semaine", "margarine"]
    # Sélectionne un mot et le divise en lettres dans un tableau
    selected_word = words[rand(0..words.length)].split('')

    # Créé un nouveau tableau vide pour contenir le mot caché
    hidden_word = Array.new

    # Ajoute autant de - au nouveau tableau qu'il y a de lettres dans le mot selectionné
    selected_word.length.times do |n|
        hidden_word.push("-")
    end

    # Nombre de vies et score au départ
    lives = 6
    score = 0

    # Boucle permettant au joueur de choisir une lettre tant qu'il a des vies et que le mot n'est pas trouvé
    while lives > 0 && (hidden_word.include? "-")
        # Compteur permettant de savoir si une lettre a été trouvé pendant le tour
        count = 0
        puts "Il vous reste #{lives} vies"
        puts hidden_word.join
        # Met la lettre entrée en minuscule
        letter = gets.chomp.downcase
        # Demande à l'utilisateur d'entrer une seule lettre
        until /[a-z]/.match(letter) && letter.length == 1
            puts "Il faut entrer une lettre uniquement"
            letter = gets.chomp.downcase
        end
        # Augmente le nombre de coups joués
        score += 1

        # Vérifie dans le mot sélectionné si la lettre saisie existe
        selected_word.each_with_index do |char, index|
            # Si elle est présente on remplace le "-" par la lettre dans le mot visible par l'utilisateur
            #Le compteur est augmenté de 1 pour éviter de perdre une vie
            if char == letter
                hidden_word[index] = char
                count += 1
            end
        end

        # Une vie est perdue si toutes les lettres n'ont pas été trouvées et que la lettre du tour n'existe pas dans le mot
        if (hidden_word.include? "-")
            lives -= 1 unless count > 0
        else 
            puts "Bravo vous avez trouvé le mot '#{selected_word.join}' en #{score} coups"
        end
    end

    # Lorsque le nombre de vies arrive à 0 la partie est finie
    if lives == 0 
        puts "Raté ! Le mot était '#{selected_word.join}''"
    end

    puts "Voulez-vous rejouer ?"
    puts "- Tapez 1 pour oui"
    puts "- Tapez 2 pour non"
    replay = gets.chomp
    # Vérification de la saisie utilisateur pour qu'elle soit 1 ou 2 uniquement
    until /[1-2]/.match(replay) && replay.length == 1 do
        puts 'Entre uniquement les chiffres 1 ou 2'
        replay = gets.chomp
    end
    # Le jeu est relancé lorsque l'utilisateur choisit la première option
    if replay == "1"
        game
    end
end

# Lance la partie
game
