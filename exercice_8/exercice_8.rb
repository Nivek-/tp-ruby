# Consignes
# depuis la saisie d'un utilisateur, convertir  la saisie pour en faire un "mot de passe" comme pour les mdp de wifi.
# exemple :  "roland moreno" deivent R0l4nd_M0r3n0"
# Convertir en lagage leetspeak uniquement les lettres ayant une correspondance en chiffre

def leetspeak(password)
    # Remplacement des lettres par un chiffre en lagage leetspeak
    pass = password.tr('abegilorsty', '48361102577')
    # Création d'un tableau contenant les mots contenus dans le mot de passe
    words = pass.split(" ")

    # Majuscule pour chaque première lettre d'un mot puis miniscule pour les autres
    words.each do |n|
        n.capitalize!
    end

    # Le tableau est converti est String avec un "_" pour lier les différents mots
    new_pass = words.join("_")
    "Le nouveau mot de passe est #{new_pass}"
end


# Demande de saisie à l'utilisateur
puts 'Choisissez un mot de passe'
name = gets.chomp

# Vérification que la saisie ne contient bien que des lettres et des espaces
# Le saisie est redemandée tant que l'utilisateur n'entre pas uniquement lettres et espaces
until /^[a-zA-Z ]*$/.match(name) do
  puts 'Utilisez uniquement des lettres, pas de caractère spécial ou d\'accent et remplacez les - par des espaces'
  name = gets.chomp
end

# Appel de la fonction leetspeak sur la saisie utilisateur affichant le nouveau mot de passe à l'utilisateur
puts leetspeak(name)