# Méthode avec un paramètre obligatoire et un optionnel
def hello (first_name, age=nil)
    # Ajout de la 2ème partie si l'age n'est pas nul
    "Je m'appelle #{first_name}" << (age.nil? ? "" : ", j'ai #{age} ans")
end

# Appel de la fonction avec 2 arguments
puts hello("Michel", 40)
# Appel de la fonction avec 1 argument
puts hello("Michel")