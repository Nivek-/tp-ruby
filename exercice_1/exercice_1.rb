# Création d'un tableau
arr = Array.new

# Ajout des valeurs allant de 1 à 100 dans le tableau
(1..100).each do |n|
    arr.push(n)
end

# Affichage des valeurs du tableau
puts arr