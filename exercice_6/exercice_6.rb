# Demande de saisie à l'utilisateur
puts 'Entrez votre nom et votre prénom'

# Retrait des espaces entre le nom et le prénom
name = gets.chomp.gsub(' ', "")

# Vérification que la saisie ne contient bien que des lettres et des espaces
# Le saisie est redemandée tant que l'utilisateur n'entre pas uniquement lettres et espaces
until /^[a-zA-Z ]*$/.match(name) do
  puts 'Utilisez uniquement des lettres et remplacez les - par des espaces'
  name = gets.chomp.gsub(' ', "")
end

# Création d'un tableau contenant chaque lettre de la saisie utilisateur
name_array = name.split('')

# Création d'un tableau contenant l'alphabet
alphabet = ('a'..'z').to_a

# Initialisation du total
total = 0

# On parcourt la saisie utilisateur et pour chaque lettre on vérifie sa correspondance dans le tableau de l'alphabet
name_array.each do |n|
    alphabet.each_with_index do |letter, index|
        # Lorsque la lettre de la saisie correspont à la lettre de l'alphabet
        if n == letter
            # Si l'index+1 est inférieur à 10
            # On récupère l'index de la lettre de l'alphabet et on lui ajoute 1 avant de l'ajouter au total
            if index < 9
                total += index+1
            # Si l'index+1 est supérieur à 10
            # On additionne les chiffres qui forment le nombre pour obtenir un chiffre qu'on ajoute au total
            else
                index_array = (index+1).to_s.split('')
                letter_value = 0
                index_array.each do |figure|
                    letter_value += figure.to_i
                end
                total += letter_value
            end
        end
    end
end

# Le total est passé sous forme de String puis sous forme de tableau avec chaque chiffre en élément du tableau
number = total.to_s.split('')

# Une boucle permettant d'additionner les valeurs du tableau tant qu'il y a plus d'un élément dans ce dernier
until number.length == 1
    # Le total est réinitialisé à chaque fois qu'on additionne les nouvelles valeurs du tableau
    total = 0
    # Chaque valeur du tableau est aujouté au total
    number.each do |n|
        total += n.to_i
    end
    # Le tableau number est réinitialisé avec le nouveau total
    number = total.to_s.split('')
end

puts "Le poids de votre nom est #{number[0]}"