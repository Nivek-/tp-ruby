# Demande de saisie à l'utilisateur
puts 'Entrez votre nom et votre prénom'

# Retrait des espaces entre le nom et le prénom
name = gets.chomp.gsub(' ', "")

# Vérification que la saisie ne contient bien que des lettres et des espaces
# Le saisie est redemandée tant que l'utilisateur n'entre pas uniquement lettres et espaces
until /^[a-zA-Z ]*$/.match(name) do
  puts 'Utilisez uniquement des lettres, pas de caractère spécial ou d\'accent et remplacez les - par des espaces'
  name = gets.chomp.gsub(' ', "")
end

# Convertit chaque lettre en son équivalent numérique et divise la chaine pour en faire un tableau 
name = name.tr('abcdefghijklmnopqrstuvwxyz', '12345678912345678912345678').split('')

# Initialisation du total
total = 0

# On convertit chaque chiffre du tableau en Integer et on l'ajoute au total
name.each do |n|
    total += n.to_i
end

# Le total est passé sous forme de String puis sous forme de tableau avec chaque chiffre en élément du tableau
number = total.to_s.split('')

# Une boucle permettant d'additionner les valeurs du tableau tant qu'il y a plus d'un élément dans ce dernier
until number.length == 1
    # Le total est réinitialisé à chaque fois qu'on additionne les nouvelles valeurs du tableau
    total = 0
    # Chaque valeur du tableau est aujouté au total
    number.each do |n|
        total += n.to_i
    end
    # Le tableau number est réinitialisé avec le nouveau total
    number = total.to_s.split('')
end

puts "Le poids de votre nom est #{number[0]}"